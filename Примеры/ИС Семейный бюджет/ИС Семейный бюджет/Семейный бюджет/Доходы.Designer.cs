﻿namespace Семейный_бюджет
{
    partial class Доходы
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Доходы));
            this.button11 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.составСемьиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.семейный_БюджетDataSet = new Семейный_бюджет.Семейный_БюджетDataSet();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.vwДоходыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_ДоходыTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_ДоходыTableAdapter();
            this.vwДоходыБольше500BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_Доходы_Больше_500TableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_Доходы_Больше_500TableAdapter();
            this.vwСамыйБольшойДоходBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_Самый_Большой_ДоходTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_Самый_Большой_ДоходTableAdapter();
            this.procДоходыОтдельногоЧленаСемьиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proc_Доходы_Отдельного_Члена_СемьиTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.proc_Доходы_Отдельного_Члена_СемьиTableAdapter();
            this.procСуммаДоходовBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proc_Сумма_ДоходовTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.proc_Сумма_ДоходовTableAdapter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.кодДоходаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодКатегорииДоходаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.категорииДоходовBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.описаниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.датаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодЧленаСемьиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.суммаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.доходыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.доходыTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.ДоходыTableAdapter();
            this.категории_ДоходовTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.Категории_ДоходовTableAdapter();
            this.состав_СемьиTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.Состав_СемьиTableAdapter();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.составСемьиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.семейный_БюджетDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwДоходыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwДоходыБольше500BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwСамыйБольшойДоходBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procДоходыОтдельногоЧленаСемьиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procСуммаДоходовBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.категорииДоходовBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.доходыBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(336, 318);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 30);
            this.button11.TabIndex = 16;
            this.button11.Text = "Вернуться";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 300);
            this.panel1.TabIndex = 14;
            this.panel1.Visible = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(152, 204);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(140, 50);
            this.button10.TabIndex = 17;
            this.button10.Text = "Выполнить живой поиск";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(152, 178);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(140, 20);
            this.textBox1.TabIndex = 16;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(152, 121);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(140, 50);
            this.button9.TabIndex = 15;
            this.button9.Text = "Отобразить все доходы";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button8.Location = new System.Drawing.Point(152, 65);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(140, 50);
            this.button8.TabIndex = 14;
            this.button8.Text = "Вывести самые большие доходы";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button7.Location = new System.Drawing.Point(152, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(140, 50);
            this.button7.TabIndex = 13;
            this.button7.Text = "Вывести доходы превышающие 500 рублей";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Роль в семье:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Имя:";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6.Location = new System.Drawing.Point(6, 226);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(140, 50);
            this.button6.TabIndex = 10;
            this.button6.Text = "Поиск доходов по указанному члену семьи";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.составСемьиBindingSource;
            this.comboBox2.DisplayMember = "Роль";
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.comboBox2.Enabled = false;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(6, 199);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(140, 21);
            this.comboBox2.TabIndex = 9;
            this.comboBox2.ValueMember = "Роль";
            // 
            // составСемьиBindingSource
            // 
            this.составСемьиBindingSource.DataMember = "Состав_Семьи";
            this.составСемьиBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // семейный_БюджетDataSet
            // 
            this.семейный_БюджетDataSet.DataSetName = "Семейный_БюджетDataSet";
            this.семейный_БюджетDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.составСемьиBindingSource;
            this.comboBox1.DisplayMember = "Имя";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 159);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.ValueMember = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "По:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "С:";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.Location = new System.Drawing.Point(6, 90);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(140, 50);
            this.button5.TabIndex = 5;
            this.button5.Text = "Поиск общей суммы доходов за указанный период";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 64);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(140, 20);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 25);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(140, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(255, 318);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 30);
            this.button4.TabIndex = 13;
            this.button4.Text = "Поиск";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(174, 318);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 30);
            this.button3.TabIndex = 12;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(93, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 30);
            this.button2.TabIndex = 11;
            this.button2.Text = "Изменить";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(12, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 30);
            this.button1.TabIndex = 10;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(759, 300);
            this.dataGridView1.TabIndex = 9;
            // 
            // vwДоходыBindingSource
            // 
            this.vwДоходыBindingSource.DataMember = "vw_Доходы";
            this.vwДоходыBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_ДоходыTableAdapter
            // 
            this.vw_ДоходыTableAdapter.ClearBeforeFill = true;
            // 
            // vwДоходыБольше500BindingSource
            // 
            this.vwДоходыБольше500BindingSource.DataMember = "vw_Доходы_Больше_500";
            this.vwДоходыБольше500BindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_Доходы_Больше_500TableAdapter
            // 
            this.vw_Доходы_Больше_500TableAdapter.ClearBeforeFill = true;
            // 
            // vwСамыйБольшойДоходBindingSource
            // 
            this.vwСамыйБольшойДоходBindingSource.DataMember = "vw_Самый_Большой_Доход";
            this.vwСамыйБольшойДоходBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_Самый_Большой_ДоходTableAdapter
            // 
            this.vw_Самый_Большой_ДоходTableAdapter.ClearBeforeFill = true;
            // 
            // procДоходыОтдельногоЧленаСемьиBindingSource
            // 
            this.procДоходыОтдельногоЧленаСемьиBindingSource.DataMember = "proc_Доходы_Отдельного_Члена_Семьи";
            this.procДоходыОтдельногоЧленаСемьиBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // proc_Доходы_Отдельного_Члена_СемьиTableAdapter
            // 
            this.proc_Доходы_Отдельного_Члена_СемьиTableAdapter.ClearBeforeFill = true;
            // 
            // procСуммаДоходовBindingSource
            // 
            this.procСуммаДоходовBindingSource.DataMember = "proc_Сумма_Доходов";
            this.procСуммаДоходовBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // proc_Сумма_ДоходовTableAdapter
            // 
            this.proc_Сумма_ДоходовTableAdapter.ClearBeforeFill = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(760, 300);
            this.panel2.TabIndex = 17;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодДоходаDataGridViewTextBoxColumn,
            this.кодКатегорииДоходаDataGridViewTextBoxColumn,
            this.описаниеDataGridViewTextBoxColumn,
            this.датаDataGridViewTextBoxColumn,
            this.кодЧленаСемьиDataGridViewTextBoxColumn,
            this.Column1,
            this.суммаDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.доходыBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(1, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(759, 300);
            this.dataGridView2.TabIndex = 7;
            // 
            // кодДоходаDataGridViewTextBoxColumn
            // 
            this.кодДоходаDataGridViewTextBoxColumn.DataPropertyName = "Код_Дохода";
            this.кодДоходаDataGridViewTextBoxColumn.HeaderText = "Код_Дохода";
            this.кодДоходаDataGridViewTextBoxColumn.Name = "кодДоходаDataGridViewTextBoxColumn";
            this.кодДоходаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодДоходаDataGridViewTextBoxColumn.Visible = false;
            // 
            // кодКатегорииДоходаDataGridViewTextBoxColumn
            // 
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.DataPropertyName = "Код_Категории_Дохода";
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.DataSource = this.категорииДоходовBindingSource;
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.HeaderText = "Категория доходов";
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.Name = "кодКатегорииДоходаDataGridViewTextBoxColumn";
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодКатегорииДоходаDataGridViewTextBoxColumn.ValueMember = "Код_Категории_Дохода";
            // 
            // категорииДоходовBindingSource
            // 
            this.категорииДоходовBindingSource.DataMember = "Категории_Доходов";
            this.категорииДоходовBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // описаниеDataGridViewTextBoxColumn
            // 
            this.описаниеDataGridViewTextBoxColumn.DataPropertyName = "Описание";
            this.описаниеDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеDataGridViewTextBoxColumn.Name = "описаниеDataGridViewTextBoxColumn";
            this.описаниеDataGridViewTextBoxColumn.ReadOnly = true;
            this.описаниеDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // датаDataGridViewTextBoxColumn
            // 
            this.датаDataGridViewTextBoxColumn.DataPropertyName = "Дата";
            this.датаDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.датаDataGridViewTextBoxColumn.Name = "датаDataGridViewTextBoxColumn";
            this.датаDataGridViewTextBoxColumn.ReadOnly = true;
            this.датаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // кодЧленаСемьиDataGridViewTextBoxColumn
            // 
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DataPropertyName = "Код_Члена_Семьи";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DataSource = this.составСемьиBindingSource;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DisplayMember = "Роль";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.HeaderText = "Роль члена семьи";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.Name = "кодЧленаСемьиDataGridViewTextBoxColumn";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.ValueMember = "Код_Члена_Семьи";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Код_Члена_Семьи";
            this.Column1.DataSource = this.составСемьиBindingSource;
            this.Column1.DisplayMember = "Имя";
            this.Column1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Column1.HeaderText = "Имя члена семьи";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.ValueMember = "Код_Члена_Семьи";
            // 
            // суммаDataGridViewTextBoxColumn
            // 
            this.суммаDataGridViewTextBoxColumn.DataPropertyName = "Сумма";
            this.суммаDataGridViewTextBoxColumn.HeaderText = "Сумма";
            this.суммаDataGridViewTextBoxColumn.Name = "суммаDataGridViewTextBoxColumn";
            this.суммаDataGridViewTextBoxColumn.ReadOnly = true;
            this.суммаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // доходыBindingSource
            // 
            this.доходыBindingSource.DataMember = "Доходы";
            this.доходыBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // доходыTableAdapter
            // 
            this.доходыTableAdapter.ClearBeforeFill = true;
            // 
            // категории_ДоходовTableAdapter
            // 
            this.категории_ДоходовTableAdapter.ClearBeforeFill = true;
            // 
            // состав_СемьиTableAdapter
            // 
            this.состав_СемьиTableAdapter.ClearBeforeFill = true;
            // 
            // Доходы
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(784, 361);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Доходы";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Доходы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Доходы_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Доходы_FormClosed);
            this.Load += new System.EventHandler(this.Доходы_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.составСемьиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.семейный_БюджетDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwДоходыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwДоходыБольше500BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwСамыйБольшойДоходBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procДоходыОтдельногоЧленаСемьиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procСуммаДоходовBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.категорииДоходовBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.доходыBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Семейный_БюджетDataSet семейный_БюджетDataSet;
        private System.Windows.Forms.BindingSource vwДоходыBindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_ДоходыTableAdapter vw_ДоходыTableAdapter;
        private System.Windows.Forms.BindingSource vwДоходыБольше500BindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_Доходы_Больше_500TableAdapter vw_Доходы_Больше_500TableAdapter;
        private System.Windows.Forms.BindingSource vwСамыйБольшойДоходBindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_Самый_Большой_ДоходTableAdapter vw_Самый_Большой_ДоходTableAdapter;
        private System.Windows.Forms.BindingSource procДоходыОтдельногоЧленаСемьиBindingSource;
        private Семейный_БюджетDataSetTableAdapters.proc_Доходы_Отдельного_Члена_СемьиTableAdapter proc_Доходы_Отдельного_Члена_СемьиTableAdapter;
        private System.Windows.Forms.BindingSource procСуммаДоходовBindingSource;
        private Семейный_БюджетDataSetTableAdapters.proc_Сумма_ДоходовTableAdapter proc_Сумма_ДоходовTableAdapter;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource доходыBindingSource;
        private Семейный_БюджетDataSetTableAdapters.ДоходыTableAdapter доходыTableAdapter;
        private System.Windows.Forms.BindingSource категорииДоходовBindingSource;
        private Семейный_БюджетDataSetTableAdapters.Категории_ДоходовTableAdapter категории_ДоходовTableAdapter;
        private System.Windows.Forms.BindingSource составСемьиBindingSource;
        private Семейный_БюджетDataSetTableAdapters.Состав_СемьиTableAdapter состав_СемьиTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодДоходаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодКатегорииДоходаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn датаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодЧленаСемьиDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn суммаDataGridViewTextBoxColumn;
    }
}