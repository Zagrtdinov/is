﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class ДоходыДобавитьИзменить : Form
    {
        public ДоходыДобавитьИзменить()
        {
            InitializeComponent();
            if (Program.ДоходыДобавитьИзменить == 1)
            {
                this.Text = "Добавление";
                textBox1.Text = Convert.ToString(Program.ДоходыДобавитьИзменитьКод + 1);
            }
            else if (Program.ДоходыДобавитьИзменить == 2)
            {
                textBox1.Text = Convert.ToString(Program.ДоходыДобавитьИзменитьКод);
                //
                textBox2.Text = Convert.ToString(Program.ДоходыДобавитьИзменитьОписание);
                dateTimePicker1.Value = Convert.ToDateTime(Program.ДоходыДобавитьИзменитьДата);
                //
                textBox4.Text = Convert.ToString(Program.ДоходыДобавитьИзменитьСумма);
                this.Text = "Изменение";
                button1.Font = new Font(this.Font, FontStyle.Strikeout);
                button1.Enabled = false;
            }
        }

        private void ДоходыДобавитьИзменить_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Доходы". При необходимости она может быть перемещена или удалена.
            this.доходыTableAdapter.Fill(this.семейный_БюджетDataSet.Доходы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Доходов". При необходимости она может быть перемещена или удалена.
            this.категории_ДоходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Доходов);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Доходы доходы = new Доходы();
            доходы.Show();
            Hide();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            int Категория = 0;
            int Член = 0;
            for (int i = 0; i < dataGridView1.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j += 1)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        if (dataGridView1.Rows[i].Cells[1].Value.ToString() == comboBox1.Text)
                        {
                            Категория = Convert.ToInt32(dataGridView1.Rows[i].Cells[0].Value);
                        }
                    }
                }
            }
            for (int i = 0; i < dataGridView2.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j += 1)
                {
                    if (dataGridView2.Rows[i].Cells[j].Value != null)
                    {
                        if (dataGridView2.Rows[i].Cells[2].Value.ToString() == comboBox2.Text && dataGridView2.Rows[i].Cells[4].Value.ToString() == comboBox3.Text)
                        {
                            Член = Convert.ToInt32(dataGridView2.Rows[i].Cells[0].Value);
                        }
                    }
                }
            }

            try
            {
                доходыTableAdapter.Insert(Convert.ToInt32(textBox1.Text), Категория, textBox2.Text, dateTimePicker1.Value, Член, Convert.ToDecimal(textBox4.Text));
                Доходы доходы = new Доходы();
                доходы.Show();
                Hide();
            }
            catch
            {
                MessageBox.Show("Необходимо заполнить все поля правильно.");
            }
        }

        private void ДоходыДобавитьИзменить_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void ДоходыДобавитьИзменить_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
