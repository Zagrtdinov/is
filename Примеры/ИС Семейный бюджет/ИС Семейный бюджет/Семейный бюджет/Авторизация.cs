﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Авторизация : Form
    {
        public Авторизация()
        {
            InitializeComponent();
        }

        private void Авторизация_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i=0;i<dataGridView1.RowCount;i+=1)
            {
                for(int j=0;j<dataGridView1.ColumnCount;j+=1)
                {
                    if(dataGridView1.Rows[i].Cells[j].Value!=null)
                    {
                        if(dataGridView1.Rows[i].Cells[5].Value.ToString()==textBox1.Text && dataGridView1.Rows[i].Cells[6].Value.ToString() == textBox2.Text)
                        {
                            Program.ФИО = Convert.ToString(dataGridView1.Rows[i].Cells[1].Value + " " + dataGridView1.Rows[i].Cells[2].Value
                                + " " + dataGridView1.Rows[i].Cells[3].Value);
                            Program.Роль = Convert.ToString(dataGridView1.Rows[i].Cells[4].Value);
                            Главная главная = new Главная();
                            главная.Show();
                            Hide();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Пользователь не найден.");
                        return;
                    }
                }
            }
        }

        private void Авторизация_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);

        }

        private void Авторизация_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
