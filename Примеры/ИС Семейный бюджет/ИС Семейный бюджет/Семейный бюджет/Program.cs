﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    static class Program
    {
        public static int РасходыДобавитьИзменить;
        public static int РасходыДобавитьИзменитьКод;
        public static int РасходыДобавитьИзменитьКатегория;
        public static string РасходыДобавитьИзменитьОписание;
        public static string РасходыДобавитьИзменитьДата;
        public static int РасходыДобавитьИзменитьЧлен;
        public static int РасходыДобавитьИзменитьСумма;
        public static string ФИО;
        public static string Роль;
        public static int ДоходыДобавитьИзменить;
        public static int ДоходыДобавитьИзменитьКод;
        public static int ДоходыДобавитьИзменитьКатегория;
        public static string ДоходыДобавитьИзменитьОписание;
        public static string ДоходыДобавитьИзменитьДата;
        public static int ДоходыДобавитьИзменитьЧлен;
        public static int ДоходыДобавитьИзменитьСумма;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Загрузка());
        }
    }
}
