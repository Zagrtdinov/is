﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Доходы : Form
    {
        public Доходы()
        {
            InitializeComponent();
        }

        private void Доходы_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Доходы_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Главная главная = new Главная();
            главная.Show();
            Hide();
        }

        private void Доходы_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Доходов". При необходимости она может быть перемещена или удалена.
            this.категории_ДоходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Доходов);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Доходы". При необходимости она может быть перемещена или удалена.
            this.доходыTableAdapter.Fill(this.семейный_БюджетDataSet.Доходы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Самый_Большой_Доход". При необходимости она может быть перемещена или удалена.
            this.vw_Самый_Большой_ДоходTableAdapter.Fill(this.семейный_БюджетDataSet.vw_Самый_Большой_Доход);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Доходы_Больше_500". При необходимости она может быть перемещена или удалена.
            this.vw_Доходы_Больше_500TableAdapter.Fill(this.семейный_БюджетDataSet.vw_Доходы_Больше_500);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Доходы". При необходимости она может быть перемещена или удалена.
            this.vw_ДоходыTableAdapter.Fill(this.семейный_БюджетDataSet.vw_Доходы);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
            button1.Font = new Font(this.Font, FontStyle.Strikeout);
            button2.Font = new Font(this.Font, FontStyle.Strikeout);
            button3.Font = new Font(this.Font, FontStyle.Strikeout);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = vwДоходыБольше500BindingSource;
            panel1.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = vwСамыйБольшойДоходBindingSource;
            panel1.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            proc_Доходы_Отдельного_Члена_СемьиTableAdapter.Fill(семейный_БюджетDataSet.proc_Доходы_Отдельного_Члена_Семьи, comboBox1.Text, comboBox2.Text);
            dataGridView1.DataSource = procДоходыОтдельногоЧленаСемьиBindingSource;
            panel1.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            proc_Сумма_ДоходовTableAdapter.Fill(семейный_БюджетDataSet.proc_Сумма_Доходов, dateTimePicker1.Value, dateTimePicker2.Value);
            dataGridView1.DataSource = procСуммаДоходовBindingSource;
            panel1.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            button1.Font = new Font(this.Font, FontStyle.Italic);
            button2.Font = new Font(this.Font, FontStyle.Italic);
            button3.Font = new Font(this.Font, FontStyle.Italic);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView2.RowCount; i += 1)
            {
                dataGridView2.Rows[i].Selected = false;
                for (int j = 0; j < dataGridView2.ColumnCount; j += 1)
                {
                    if (dataGridView2.Rows[i].Cells[j].Value != null)
                    {
                        if (dataGridView2.Rows[i].Cells[j].Value.ToString().Contains(textBox1.Text))
                        {
                            dataGridView2.Rows[i].Selected = true;
                        }
                    }
                }
            }
            panel1.Visible = false;
            panel2.Visible = true;
            button1.Font = new Font(this.Font, FontStyle.Italic);
            button2.Font = new Font(this.Font, FontStyle.Italic);
            button3.Font = new Font(this.Font, FontStyle.Italic);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView2.Rows.RemoveAt(dataGridView2.SelectedRows[0].Index);
                доходыTableAdapter.Update(семейный_БюджетDataSet);
            }
            catch
            {
                MessageBox.Show("Необходимо выбрать строку.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView2.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j += 1)
                {
                    if (dataGridView2.Rows[i].Cells[j].Value != null)
                    {
                        Program.ДоходыДобавитьИзменитьКод = Convert.ToInt32(dataGridView2.Rows[i].Cells[0].Value);
                    }
                }
            }
            Program.ДоходыДобавитьИзменить = 1;
            ДоходыДобавитьИзменить доходыДобавитьИзменить = new ДоходыДобавитьИзменить();
            доходыДобавитьИзменить.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Program.ДоходыДобавитьИзменитьКод = dataGridView2.SelectedRows[0].Index;
                Program.ДоходыДобавитьИзменитьКатегория = Convert.ToInt32(dataGridView2.Rows[Program.ДоходыДобавитьИзменитьКод].Cells[1].Value);
                Program.ДоходыДобавитьИзменитьОписание = Convert.ToString(dataGridView2.Rows[Program.ДоходыДобавитьИзменитьКод].Cells[2].Value);
                Program.ДоходыДобавитьИзменитьДата = Convert.ToString(dataGridView2.Rows[Program.ДоходыДобавитьИзменитьКод].Cells[3].Value);
                Program.ДоходыДобавитьИзменитьЧлен = Convert.ToInt32(dataGridView2.Rows[Program.ДоходыДобавитьИзменитьКод].Cells[4].Value);
                Program.ДоходыДобавитьИзменитьСумма = Convert.ToInt32(dataGridView2.Rows[Program.ДоходыДобавитьИзменитьКод].Cells[6].Value);
                Program.ДоходыДобавитьИзменить = 2;
                dataGridView2.Rows.RemoveAt(dataGridView2.SelectedRows[0].Index);
                доходыTableAdapter.Update(семейный_БюджетDataSet);
                ДоходыДобавитьИзменить доходыДобавитьИзменить = new ДоходыДобавитьИзменить();
                доходыДобавитьИзменить.Show();
                Hide();
            }
            catch
            {
                MessageBox.Show("Необходимо выбрать строку.");
            }
        }
    }
}
