﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Главная : Form
    {
        public Главная()
        {
            InitializeComponent();
            label1.Text = "Добро пожаловать"+
                Environment.NewLine+
                "ФИО: " + Environment.NewLine + Program.ФИО+Environment.NewLine+
                "Роль в семье: " + Environment.NewLine + Program.Роль;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Расходы расходы = new Расходы();
            расходы.Show();
            Hide();
        }

        private void Главная_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Бюджет бюджет = new Бюджет();
            бюджет.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Доходы доходы = new Доходы();
            доходы.Show();
            Hide();
        }

        private void Главная_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Категории категории = new Категории();
            категории.Show();
            Hide();
        }
    }
}
