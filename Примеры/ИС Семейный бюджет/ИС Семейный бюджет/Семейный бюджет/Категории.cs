﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Категории : Form
    {
        public Категории()
        {
            InitializeComponent();
        }

        private void Категории_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Доходов". При необходимости она может быть перемещена или удалена.
            this.категории_ДоходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Доходов);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Расходов". При необходимости она может быть перемещена или удалена.
            this.категории_РасходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Расходов);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Расходы". При необходимости она может быть перемещена или удалена.

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Главная главная = new Главная();
            главная.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                категории_ДоходовTableAdapter.Update(семейный_БюджетDataSet);
                категории_РасходовTableAdapter.Update(семейный_БюджетDataSet);
            }
            catch
            {
                MessageBox.Show("Категория используется.");
                // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Доходов". При необходимости она может быть перемещена или удалена.
                this.категории_ДоходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Доходов);
                // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Расходов". При необходимости она может быть перемещена или удалена.
                this.категории_РасходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Расходов);
            }
        }

    }
}
