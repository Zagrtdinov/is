﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class РасходыДобавитьИзменить : Form
    {
        public РасходыДобавитьИзменить()
        {
            InitializeComponent();
            if(Program.РасходыДобавитьИзменить==1)
            {
                this.Text = "Добавление";
                textBox1.Text = Convert.ToString(Program.РасходыДобавитьИзменитьКод + 1);
            }
            else if(Program.РасходыДобавитьИзменить == 2)
            {
                textBox1.Text = Convert.ToString(Program.РасходыДобавитьИзменитьКод);
                //
                textBox2.Text= Convert.ToString(Program.РасходыДобавитьИзменитьОписание);
                dateTimePicker1.Value = Convert.ToDateTime(Program.РасходыДобавитьИзменитьДата);
                //
                textBox4.Text= Convert.ToString(Program.РасходыДобавитьИзменитьСумма);
                this.Text = "Изменение";
                button1.Font = new Font(this.Font, FontStyle.Strikeout);
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Расходы расходы = new Расходы();
            расходы.Show();
            Hide();
        }

        private void РасходыДобавитьИзменить_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Расходы". При необходимости она может быть перемещена или удалена.
            this.расходыTableAdapter.Fill(this.семейный_БюджетDataSet.Расходы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Расходов". При необходимости она может быть перемещена или удалена.
            this.категории_РасходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Расходов);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);

        }

        private void button11_Click(object sender, EventArgs e)
        {
            int Категория = 0;
            int Член = 0;
            for (int i = 0; i < dataGridView1.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView1.ColumnCount; j += 1)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        if (dataGridView1.Rows[i].Cells[1].Value.ToString()==comboBox1.Text)
                        {
                            Категория = Convert.ToInt32(dataGridView1.Rows[i].Cells[0].Value);
                        }
                    }
                }
            }
            for (int i = 0; i < dataGridView2.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j += 1)
                {
                    if (dataGridView2.Rows[i].Cells[j].Value != null)
                    {
                        if (dataGridView2.Rows[i].Cells[2].Value.ToString() == comboBox2.Text && dataGridView2.Rows[i].Cells[4].Value.ToString() == comboBox3.Text)
                        {
                            Член = Convert.ToInt32(dataGridView2.Rows[i].Cells[0].Value);
                        }
                    }
                }
            }

                try
                {
                    расходыTableAdapter.Insert(Convert.ToInt32(textBox1.Text), Категория, textBox2.Text, dateTimePicker1.Value, Член, Convert.ToDecimal(textBox4.Text));
                    Расходы расходы = new Расходы();
                    расходы.Show();
                    Hide();
                }
                catch
                {
                    MessageBox.Show("Необходимо заполнить все поля правильно.");
                }
            
        }

        private void РасходыДобавитьИзменить_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void РасходыДобавитьИзменить_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
