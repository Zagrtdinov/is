﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Расходы : Form
    {
        public Расходы()
        {
            InitializeComponent();        
        }

        private void Расходы_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Расходы". При необходимости она может быть перемещена или удалена.
            this.vw_РасходыTableAdapter.Fill(this.семейный_БюджетDataSet.vw_Расходы);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Расходы_Больше_500". При необходимости она может быть перемещена или удалена.
            this.vw_Расходы_Больше_500TableAdapter.Fill(this.семейный_БюджетDataSet.vw_Расходы_Больше_500);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.vw_Самый_Большой_Расход". При необходимости она может быть перемещена или удалена.
            this.vw_Самый_Большой_РасходTableAdapter.Fill(this.семейный_БюджетDataSet.vw_Самый_Большой_Расход);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Категории_Расходов". При необходимости она может быть перемещена или удалена.
            this.категории_РасходовTableAdapter.Fill(this.семейный_БюджетDataSet.Категории_Расходов);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Расходы". При необходимости она может быть перемещена или удалена.
            this.расходыTableAdapter.Fill(this.семейный_БюджетDataSet.Расходы);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            proc_Сумма_РасходовTableAdapter.Fill(семейный_БюджетDataSet.proc_Сумма_Расходов, dateTimePicker1.Value, dateTimePicker2.Value);
            dataGridView1.DataSource = procСуммаРасходовBindingSource;
            panel1.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
            button1.Font = new Font(this.Font, FontStyle.Strikeout);
            button2.Font = new Font(this.Font, FontStyle.Strikeout);
            button3.Font = new Font(this.Font, FontStyle.Strikeout);
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            proc_Расходы_Отдельного_Члена_СемьиTableAdapter.Fill(семейный_БюджетDataSet.proc_Расходы_Отдельного_Члена_Семьи, comboBox1.Text, comboBox2.Text);
            dataGridView1.DataSource = procРасходыОтдельногоЧленаСемьиBindingSource;
            panel1.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = vwРасходыБольше500BindingSource;
            panel1.Visible = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = vwСамыйБольшойРасходBindingSource;
            panel1.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            button1.Font = new Font(this.Font, FontStyle.Italic);
            button2.Font = new Font(this.Font, FontStyle.Italic);
            button3.Font = new Font(this.Font, FontStyle.Italic);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            for(int i=0;i<dataGridView2.RowCount;i+=1)
            {
                dataGridView2.Rows[i].Selected = false;
                for(int j=0;j<dataGridView2.ColumnCount;j+=1)
                {
                    if(dataGridView2.Rows[i].Cells[j].Value!=null)
                    {
                        if (dataGridView2.Rows[i].Cells[j].Value.ToString().Contains(textBox1.Text))
                        {
                            dataGridView2.Rows[i].Selected = true;
                        }
                    }
                }
            }
            panel1.Visible = false;
            panel2.Visible = true;
            button1.Font = new Font(this.Font, FontStyle.Italic);
            button2.Font = new Font(this.Font, FontStyle.Italic);
            button3.Font = new Font(this.Font, FontStyle.Italic);
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Главная главная = new Главная();
            главная.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView2.Rows.RemoveAt(dataGridView2.SelectedRows[0].Index);
                расходыTableAdapter.Update(семейный_БюджетDataSet);
            }
            catch
            {
                MessageBox.Show("Необходимо выбрать строку.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            for (int i = 0; i < dataGridView2.RowCount; i += 1)
            {
                for (int j = 0; j < dataGridView2.ColumnCount; j += 1)
                {
                    if (dataGridView2.Rows[i].Cells[j].Value != null)
                    {
                        Program.РасходыДобавитьИзменитьКод = Convert.ToInt32(dataGridView2.Rows[i].Cells[0].Value);
                    }
                }
            }
            Program.РасходыДобавитьИзменить = 1;
            РасходыДобавитьИзменить расходыДобавитьИзменить = new РасходыДобавитьИзменить();
            расходыДобавитьИзменить.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Program.РасходыДобавитьИзменитьКод = dataGridView2.SelectedRows[0].Index;
                Program.РасходыДобавитьИзменитьКатегория = Convert.ToInt32(dataGridView2.Rows[Program.РасходыДобавитьИзменитьКод].Cells[1].Value);
                Program.РасходыДобавитьИзменитьОписание = Convert.ToString(dataGridView2.Rows[Program.РасходыДобавитьИзменитьКод].Cells[2].Value);
                Program.РасходыДобавитьИзменитьДата = Convert.ToString(dataGridView2.Rows[Program.РасходыДобавитьИзменитьКод].Cells[3].Value);
                Program.РасходыДобавитьИзменитьЧлен = Convert.ToInt32(dataGridView2.Rows[Program.РасходыДобавитьИзменитьКод].Cells[4].Value);
                Program.РасходыДобавитьИзменитьСумма = Convert.ToInt32(dataGridView2.Rows[Program.РасходыДобавитьИзменитьКод].Cells[6].Value);
                Program.РасходыДобавитьИзменить = 2;
                dataGridView2.Rows.RemoveAt(dataGridView2.SelectedRows[0].Index);
                расходыTableAdapter.Update(семейный_БюджетDataSet);
                РасходыДобавитьИзменить расходыДобавитьИзменить = new РасходыДобавитьИзменить();
                расходыДобавитьИзменить.Show();
                Hide();
            }
            catch
            {
                MessageBox.Show("Необходимо выбрать строку.");
            }
        }

        private void Расходы_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Расходы_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
