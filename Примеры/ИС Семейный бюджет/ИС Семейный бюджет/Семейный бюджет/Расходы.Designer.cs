﻿namespace Семейный_бюджет
{
    partial class Расходы
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Расходы));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.семейный_БюджетDataSet = new Семейный_бюджет.Семейный_БюджетDataSet();
            this.расходыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.расходыTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.РасходыTableAdapter();
            this.категорииРасходовBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.категории_РасходовTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.Категории_РасходовTableAdapter();
            this.составСемьиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.состав_СемьиTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.Состав_СемьиTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.procСуммаРасходовBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proc_Сумма_РасходовTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.proc_Сумма_РасходовTableAdapter();
            this.procРасходыОтдельногоЧленаСемьиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proc_Расходы_Отдельного_Члена_СемьиTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.proc_Расходы_Отдельного_Члена_СемьиTableAdapter();
            this.vwСамыйБольшойРасходBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_Самый_Большой_РасходTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_Самый_Большой_РасходTableAdapter();
            this.vwРасходыБольше500BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_Расходы_Больше_500TableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_Расходы_Больше_500TableAdapter();
            this.vwРасходыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vw_РасходыTableAdapter = new Семейный_бюджет.Семейный_БюджетDataSetTableAdapters.vw_РасходыTableAdapter();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.кодРасходаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодКатегорииРасходаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.описаниеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.датаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодЧленаСемьиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.суммаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.семейный_БюджетDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.категорииРасходовBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.составСемьиBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.procСуммаРасходовBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procРасходыОтдельногоЧленаСемьиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwСамыйБольшойРасходBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwРасходыБольше500BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwРасходыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(759, 300);
            this.dataGridView1.TabIndex = 0;
            // 
            // семейный_БюджетDataSet
            // 
            this.семейный_БюджетDataSet.DataSetName = "Семейный_БюджетDataSet";
            this.семейный_БюджетDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // расходыBindingSource
            // 
            this.расходыBindingSource.DataMember = "Расходы";
            this.расходыBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // расходыTableAdapter
            // 
            this.расходыTableAdapter.ClearBeforeFill = true;
            // 
            // категорииРасходовBindingSource
            // 
            this.категорииРасходовBindingSource.DataMember = "Категории_Расходов";
            this.категорииРасходовBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // категории_РасходовTableAdapter
            // 
            this.категории_РасходовTableAdapter.ClearBeforeFill = true;
            // 
            // составСемьиBindingSource
            // 
            this.составСемьиBindingSource.DataMember = "Состав_Семьи";
            this.составСемьиBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // состав_СемьиTableAdapter
            // 
            this.состав_СемьиTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(12, 319);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(93, 319);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "Изменить";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(174, 319);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 30);
            this.button3.TabIndex = 3;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(255, 319);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 30);
            this.button4.TabIndex = 4;
            this.button4.Text = "Поиск";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 300);
            this.panel1.TabIndex = 5;
            this.panel1.Visible = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(152, 204);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(140, 50);
            this.button10.TabIndex = 17;
            this.button10.Text = "Выполнить живой поиск";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(152, 178);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(140, 20);
            this.textBox1.TabIndex = 16;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(152, 121);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(140, 50);
            this.button9.TabIndex = 15;
            this.button9.Text = "Отобразить все расходы";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button8.Location = new System.Drawing.Point(152, 65);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(140, 50);
            this.button8.TabIndex = 14;
            this.button8.Text = "Вывести самые большие расходы";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button7.Location = new System.Drawing.Point(152, 9);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(140, 50);
            this.button7.TabIndex = 13;
            this.button7.Text = "Вывести расходы превышающие 500 рублей";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Роль в семье:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Имя:";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6.Location = new System.Drawing.Point(6, 226);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(140, 50);
            this.button6.TabIndex = 10;
            this.button6.Text = "Поиск расходов по указанному члену семьи";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.DataSource = this.составСемьиBindingSource;
            this.comboBox2.DisplayMember = "Роль";
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.comboBox2.Enabled = false;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(6, 199);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(140, 21);
            this.comboBox2.TabIndex = 9;
            this.comboBox2.ValueMember = "Роль";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.составСемьиBindingSource;
            this.comboBox1.DisplayMember = "Имя";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 159);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.ValueMember = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "По:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "С:";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.Location = new System.Drawing.Point(6, 90);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(140, 50);
            this.button5.TabIndex = 5;
            this.button5.Text = "Поиск общей суммы расходов за указанный период";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(6, 64);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(140, 20);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 25);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(140, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // procСуммаРасходовBindingSource
            // 
            this.procСуммаРасходовBindingSource.DataMember = "proc_Сумма_Расходов";
            this.procСуммаРасходовBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // proc_Сумма_РасходовTableAdapter
            // 
            this.proc_Сумма_РасходовTableAdapter.ClearBeforeFill = true;
            // 
            // procРасходыОтдельногоЧленаСемьиBindingSource
            // 
            this.procРасходыОтдельногоЧленаСемьиBindingSource.DataMember = "proc_Расходы_Отдельного_Члена_Семьи";
            this.procРасходыОтдельногоЧленаСемьиBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // proc_Расходы_Отдельного_Члена_СемьиTableAdapter
            // 
            this.proc_Расходы_Отдельного_Члена_СемьиTableAdapter.ClearBeforeFill = true;
            // 
            // vwСамыйБольшойРасходBindingSource
            // 
            this.vwСамыйБольшойРасходBindingSource.DataMember = "vw_Самый_Большой_Расход";
            this.vwСамыйБольшойРасходBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_Самый_Большой_РасходTableAdapter
            // 
            this.vw_Самый_Большой_РасходTableAdapter.ClearBeforeFill = true;
            // 
            // vwРасходыБольше500BindingSource
            // 
            this.vwРасходыБольше500BindingSource.DataMember = "vw_Расходы_Больше_500";
            this.vwРасходыБольше500BindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_Расходы_Больше_500TableAdapter
            // 
            this.vw_Расходы_Больше_500TableAdapter.ClearBeforeFill = true;
            // 
            // vwРасходыBindingSource
            // 
            this.vwРасходыBindingSource.DataMember = "vw_Расходы";
            this.vwРасходыBindingSource.DataSource = this.семейный_БюджетDataSet;
            // 
            // vw_РасходыTableAdapter
            // 
            this.vw_РасходыTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодРасходаDataGridViewTextBoxColumn,
            this.кодКатегорииРасходаDataGridViewTextBoxColumn,
            this.описаниеDataGridViewTextBoxColumn,
            this.датаDataGridViewTextBoxColumn,
            this.кодЧленаСемьиDataGridViewTextBoxColumn,
            this.Column1,
            this.суммаDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.расходыBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(1, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(759, 300);
            this.dataGridView2.TabIndex = 6;
            // 
            // кодРасходаDataGridViewTextBoxColumn
            // 
            this.кодРасходаDataGridViewTextBoxColumn.DataPropertyName = "Код_Расхода";
            this.кодРасходаDataGridViewTextBoxColumn.HeaderText = "Код_Расхода";
            this.кодРасходаDataGridViewTextBoxColumn.Name = "кодРасходаDataGridViewTextBoxColumn";
            this.кодРасходаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодРасходаDataGridViewTextBoxColumn.Visible = false;
            // 
            // кодКатегорииРасходаDataGridViewTextBoxColumn
            // 
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.DataPropertyName = "Код_Категории_Расхода";
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.DataSource = this.категорииРасходовBindingSource;
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.HeaderText = "Категория расходов";
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.Name = "кодКатегорииРасходаDataGridViewTextBoxColumn";
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодКатегорииРасходаDataGridViewTextBoxColumn.ValueMember = "Код_Категории_Расхода";
            // 
            // описаниеDataGridViewTextBoxColumn
            // 
            this.описаниеDataGridViewTextBoxColumn.DataPropertyName = "Описание";
            this.описаниеDataGridViewTextBoxColumn.HeaderText = "Описание";
            this.описаниеDataGridViewTextBoxColumn.Name = "описаниеDataGridViewTextBoxColumn";
            this.описаниеDataGridViewTextBoxColumn.ReadOnly = true;
            this.описаниеDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // датаDataGridViewTextBoxColumn
            // 
            this.датаDataGridViewTextBoxColumn.DataPropertyName = "Дата";
            this.датаDataGridViewTextBoxColumn.HeaderText = "Дата";
            this.датаDataGridViewTextBoxColumn.Name = "датаDataGridViewTextBoxColumn";
            this.датаDataGridViewTextBoxColumn.ReadOnly = true;
            this.датаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // кодЧленаСемьиDataGridViewTextBoxColumn
            // 
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DataPropertyName = "Код_Члена_Семьи";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DataSource = this.составСемьиBindingSource;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DisplayMember = "Роль";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.HeaderText = "Роль члена семьи";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.Name = "кодЧленаСемьиDataGridViewTextBoxColumn";
            this.кодЧленаСемьиDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодЧленаСемьиDataGridViewTextBoxColumn.ValueMember = "Код_Члена_Семьи";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Код_Члена_Семьи";
            this.Column1.DataSource = this.составСемьиBindingSource;
            this.Column1.DisplayMember = "Имя";
            this.Column1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Column1.HeaderText = "Имя члена семьи";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.ValueMember = "Код_Члена_Семьи";
            // 
            // суммаDataGridViewTextBoxColumn
            // 
            this.суммаDataGridViewTextBoxColumn.DataPropertyName = "Сумма";
            this.суммаDataGridViewTextBoxColumn.HeaderText = "Сумма";
            this.суммаDataGridViewTextBoxColumn.Name = "суммаDataGridViewTextBoxColumn";
            this.суммаDataGridViewTextBoxColumn.ReadOnly = true;
            this.суммаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Location = new System.Drawing.Point(12, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(760, 300);
            this.panel2.TabIndex = 7;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.ForeColor = System.Drawing.Color.Black;
            this.button11.Location = new System.Drawing.Point(336, 319);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 30);
            this.button11.TabIndex = 8;
            this.button11.Text = "Вернуться";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // Расходы
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(784, 361);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Расходы";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расходы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Расходы_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Расходы_FormClosed);
            this.Load += new System.EventHandler(this.Расходы_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.семейный_БюджетDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.расходыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.категорииРасходовBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.составСемьиBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.procСуммаРасходовBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procРасходыОтдельногоЧленаСемьиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwСамыйБольшойРасходBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwРасходыБольше500BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwРасходыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private Семейный_БюджетDataSet семейный_БюджетDataSet;
        private System.Windows.Forms.BindingSource расходыBindingSource;
        private Семейный_БюджетDataSetTableAdapters.РасходыTableAdapter расходыTableAdapter;
        private System.Windows.Forms.BindingSource категорииРасходовBindingSource;
        private Семейный_БюджетDataSetTableAdapters.Категории_РасходовTableAdapter категории_РасходовTableAdapter;
        private System.Windows.Forms.BindingSource составСемьиBindingSource;
        private Семейный_БюджетDataSetTableAdapters.Состав_СемьиTableAdapter состав_СемьиTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.BindingSource procСуммаРасходовBindingSource;
        private Семейный_БюджетDataSetTableAdapters.proc_Сумма_РасходовTableAdapter proc_Сумма_РасходовTableAdapter;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.BindingSource procРасходыОтдельногоЧленаСемьиBindingSource;
        private Семейный_БюджетDataSetTableAdapters.proc_Расходы_Отдельного_Члена_СемьиTableAdapter proc_Расходы_Отдельного_Члена_СемьиTableAdapter;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.BindingSource vwСамыйБольшойРасходBindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_Самый_Большой_РасходTableAdapter vw_Самый_Большой_РасходTableAdapter;
        private System.Windows.Forms.BindingSource vwРасходыБольше500BindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_Расходы_Больше_500TableAdapter vw_Расходы_Больше_500TableAdapter;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.BindingSource vwРасходыBindingSource;
        private Семейный_БюджетDataSetTableAdapters.vw_РасходыTableAdapter vw_РасходыTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодРасходаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодКатегорииРасходаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn описаниеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn датаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодЧленаСемьиDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn суммаDataGridViewTextBoxColumn;
    }
}