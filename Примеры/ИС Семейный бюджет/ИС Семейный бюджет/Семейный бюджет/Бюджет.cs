﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Семейный_бюджет
{
    public partial class Бюджет : Form
    {
        public Бюджет()
        {
            InitializeComponent();
        }

        private void Бюджет_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "семейный_БюджетDataSet.Состав_Семьи". При необходимости она может быть перемещена или удалена.
            this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                состав_СемьиTableAdapter.Update(семейный_БюджетDataSet);
            }
            catch
            {
                MessageBox.Show("Член семьи используется.");
                this.состав_СемьиTableAdapter.Fill(this.семейный_БюджетDataSet.Состав_Семьи);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Главная главная = new Главная();
            главная.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView2.Visible = true;
            proc_БюджетTableAdapter.Fill(семейный_БюджетDataSet.proc_Бюджет,dateTimePicker1.Value,dateTimePicker2.Value);
            dataGridView2.DataSource = procБюджетBindingSource;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView2.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView2.Visible = true;
            proc_Бюджет_Отдельного_Члена_СемьиTableAdapter.Fill(семейный_БюджетDataSet.proc_Бюджет_Отдельного_Члена_Семьи, comboBox1.Text, comboBox2.Text);
            dataGridView2.DataSource = procБюджетОтдельногоЧленаСемьиBindingSource;
        }

        private void Бюджет_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Бюджет_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
