﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;

namespace pp
{
    public partial class Меню : Form
    {
        private _Document GetDoc(string path)
        {
            Word._Application oWord = new Word.Application();
            _Document oDoc = oWord.Documents.Add(path);
            SetTemplate(oDoc);
            return oDoc;
        }

        private void SetTemplate(Word._Document oDoc)
        {
            oDoc.Bookmarks["ref1"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[0].Value.ToString();
            oDoc.Bookmarks["ref2"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[1].Value.ToString();
            oDoc.Bookmarks["ref3"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[10].Value.ToString();
            oDoc.Bookmarks["ref4"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[4].Value.ToString();
            oDoc.Bookmarks["ref5"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[5].Value.ToString();
            oDoc.Bookmarks["ref6"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[10].Value.ToString();
            oDoc.Bookmarks["ref7"].Range.Text = DateTime.Now.ToLongDateString();
            oDoc.Bookmarks["ref8"].Range.Text = DateTime.Now.ToLongDateString();
            oDoc.Bookmarks["ref9"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[1].Value.ToString();
            oDoc.Bookmarks["ref10"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[3].Value.ToString();
            oDoc.Bookmarks["ref11"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[6].Value.ToString();
            oDoc.Bookmarks["ref12"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[7].Value.ToString();
            oDoc.Bookmarks["ref13"].Range.Text = dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[8].Value.ToString();

        }
        public Меню()
        {
            InitializeComponent();
        }

        private void Меню_Load(object sender, EventArgs e)
        {
            this.договорыTableAdapter.Fill(this.dataDataSet.Договоры);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            договорыTableAdapter.Insert(textBox1.Text, comboBox1.Text, dateTimePicker1.Value.ToShortDateString(), dateTimePicker2.Value.ToLongDateString(),
                dateTimePicker3.Value.ToLongDateString(), maskedTextBox1.Text, maskedTextBox2.Text, textBox2.Text, maskedTextBox3.Text,
                textBox3.Text, "Ожидается подписание договора");
            this.договорыTableAdapter.Fill(this.dataDataSet.Договоры);
            MessageBox.Show("Добавление прошло успешно");
        }

        internal void color()
        {
            for(int i=0;i<dataGridView1.RowCount;i+=1)
            {
                for(int j=0;j<dataGridView1.ColumnCount;j+=1)
                {
                    if (dataGridView1.Rows[i].Cells[5].Value != null)
                    {
                        if (dataGridView1.Rows[i].Cells[5].Value.ToString() == DateTime.Now.AddDays(1).ToLongDateString())
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
                        }
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _Document oDoc = GetDoc(System.Windows.Forms.Application.StartupPath+@"\Шаблоны\договор.doc");
            oDoc.SaveAs(System.Windows.Forms.Application.StartupPath + @"\Договоры\договор"+dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[10].Value.ToString()+".doc");
            oDoc.Close();
            MessageBox.Show("Договор сохранен в "+ System.Windows.Forms.Application.StartupPath + @"\Договоры");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            color();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[11].Value = "Договор анулирован";
            MessageBox.Show("Статус успешно изменен");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            договорыTableAdapter.Update(dataDataSet.Договоры);
            MessageBox.Show("Сохранения прошли успешно");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows[dataGridView1.SelectedRows[0].Index].Cells[11].Value = "Договор подписан";
            MessageBox.Show("Статус успешно изменен");
        }
    }
}
