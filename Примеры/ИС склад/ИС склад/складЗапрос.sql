create table ����������
(
���_���������� int primary key identity,
������� varchar(255),
��� varchar(255),
�������� varchar(255),
�����  varchar(255) unique,
������ varchar(255)
)
go

create table ������
(
���_������ int primary key identity,
������������_������ varchar(255),
����������_��_������ int
)
go

create table ��������
(
���_�������� int primary key identity,
���_������ int foreign key references ������ (���_������),
����_�������� varchar(255),
����������_�_�������� int
)
go

create table �����
(
���_������ int primary key identity,
���_������ int foreign key references ������ (���_������),
����_������ varchar(255),
����������_�_������ int
)
go

create trigger i_��������
on ��������
for insert
as
declare @id int = (select ���_������ from inserted)
declare @count int = (select ����������_�_�������� from inserted)
update ������
set ����������_��_������ = ����������_��_������ + @count
where ���_������ = @id
go

create trigger i_�����
on �����
for insert
as
declare @id int = (select ���_������ from inserted)
declare @count int = (select ����������_�_������ from inserted)
if @count<=(select ����������_��_������ from ������ where ���_������=@id)
begin
update ������
set ����������_��_������ = ����������_��_������ - @count
where ���_������ = @id
end
else
begin
rollback transaction
end
go

create view vw_��������
as
select ����_�������� as [���� ��������], ������.������������_������ as [������������ ������], ����������_�_�������� as [���������� ������]
from �������� inner join ������ on ��������.���_������=������.���_������
go

create view vw_�����
as
select ����_������ as [���� ������], ������.������������_������ as [������������ ������], ����������_�_������ as
[���������� ������]
from ����� inner join ������ on �����.���_������=������.���_������
go

create view vw_������
as
select ������.���_������ as [����� ������], ������������_������ as [������������ ������], ����������_��_������ as [���������� ������]
from ������
go

create view vw_��������_�����
as
select ������.������������_������ as [������������ ������], ����_�������� as [���� ��������],
 ����������_�_�������� as [���������� ������]
from �������� inner join ������ on ��������.���_������=������.���_������
where MONTH(GETDATE())=MONTH(����_��������) and YEAR(GETDATE())=YEAR(����_��������)
go

create view vw_�����_�����
as
select ������.������������_������ as [������������ ������], ����_������ as [���� ������ �� ������],
 ����������_�_������ as [���������� ������]
from ����� inner join ������ on �����.���_������=������.���_������
where MONTH(GETDATE())=MONTH(����_������) and YEAR(GETDATE())=YEAR(����_������)
go

create view vw_����������_������
as
select ������������_������ as [������������ ������], ����������_��_������ as [����������]
from ������
where ����������_��_������=0
go

create view vw_����������_������1
as
select ������������_������ as [������������ ������], ����������_��_������ as [����������]
from ������
where ����������_��_������>0
go