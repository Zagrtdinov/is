﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace склад
{
    /// <summary>
    /// Логика взаимодействия для меню.xaml
    /// </summary>
    public partial class меню : Window
    {

        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;
        DataTable dateTable;
        string query;
        string connectString = "server=localhost; database=учёт_склада; integrated security=true";

        public меню()
        {
            InitializeComponent();
            try
            {
                comboBox2.Visibility = Visibility.Hidden;
                button1.Visibility = Visibility.Hidden;
                insertTovar.Visibility = Visibility.Hidden;

                comboBox1.Visibility = Visibility.Hidden;
                textBox1.Visibility = Visibility.Hidden;
                button2.Visibility = Visibility.Hidden;

                comboBox3.Visibility = Visibility.Hidden;
                textBox3.Visibility = Visibility.Hidden;
                button3.Visibility = Visibility.Hidden;

                t1.Visibility = Visibility.Hidden;
                t2.Visibility = Visibility.Hidden;
                t3.Visibility = Visibility.Hidden;
                t4.Visibility = Visibility.Hidden;
                t5.Visibility = Visibility.Hidden;

                connection = new SqlConnection(connectString);
                query = "select * from товары";
                connection.Open();
                adapter = new SqlDataAdapter(query, connection);
                DataSet set = new DataSet();
                adapter.Fill(set, "t");
                comboBox1.ItemsSource = set.Tables["t"].DefaultView;
                comboBox1.DisplayMemberPath = "наименование_товара";
                comboBox3.ItemsSource = set.Tables["t"].DefaultView;
                comboBox3.DisplayMemberPath = "наименование_товара";
                comboBox2.ItemsSource = set.Tables["t"].DefaultView;
                comboBox2.DisplayMemberPath = "код_товара";
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox comboBox = (ComboBox)sender;
                ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
                if (selectedItem.Content.ToString() == "Поставки")
                {
                    query = "select * from vw_поставки";
                    button1.Visibility = Visibility.Hidden;
                    insertTovar.Visibility = Visibility.Hidden;

                    comboBox1.Visibility = Visibility.Visible;
                    textBox1.Visibility = Visibility.Visible;
                    button2.Visibility = Visibility.Visible;

                    t2.Visibility = Visibility.Visible;
                    t3.Visibility = Visibility.Visible;

                    t1.Visibility = Visibility.Hidden;
                    t4.Visibility = Visibility.Hidden;
                    t5.Visibility = Visibility.Hidden;

                    comboBox3.Visibility = Visibility.Hidden;
                    textBox3.Visibility = Visibility.Hidden;
                    button3.Visibility = Visibility.Hidden;
                }
                else if (selectedItem.Content.ToString() == "Вывоз со склада")
                {
                    query = "select * from vw_вывоз";
                    button1.Visibility = Visibility.Hidden;
                    insertTovar.Visibility = Visibility.Hidden;

                    comboBox3.Visibility = Visibility.Visible;
                    textBox3.Visibility = Visibility.Visible;
                    button3.Visibility = Visibility.Visible;

                    t4.Visibility = Visibility.Visible;
                    t5.Visibility = Visibility.Visible;

                    t1.Visibility = Visibility.Hidden;
                    t2.Visibility = Visibility.Hidden;
                    t3.Visibility = Visibility.Hidden;

                    comboBox1.Visibility = Visibility.Hidden;
                    textBox1.Visibility = Visibility.Hidden;
                    button2.Visibility = Visibility.Hidden;
                }
                else if (selectedItem.Content.ToString() == "Товары на складе")
                {
                    query = "select * from vw_товары";
                    button1.Visibility = Visibility.Visible;
                    insertTovar.Visibility = Visibility.Visible;

                    comboBox1.Visibility = Visibility.Hidden;
                    textBox1.Visibility = Visibility.Hidden;
                    button2.Visibility = Visibility.Hidden;

                    t1.Visibility = Visibility.Visible;

                    t2.Visibility = Visibility.Hidden;
                    t3.Visibility = Visibility.Hidden;
                    t4.Visibility = Visibility.Hidden;
                    t5.Visibility = Visibility.Hidden;

                    comboBox3.Visibility = Visibility.Hidden;
                    textBox3.Visibility = Visibility.Hidden;
                    button3.Visibility = Visibility.Hidden;
                }
                connection = new SqlConnection(connectString);
                connection.Open();
                command = new SqlCommand(query, connection);
                dateTable = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dateTable);
                dataGrid1.ItemsSource = dateTable.DefaultView;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                connection = new SqlConnection(connectString);
                string queryInsert = "insert into товары values (@value1, @value2)";
                command = new SqlCommand(queryInsert, connection);
                command.Parameters.AddWithValue("@value1", insertTovar.Text);
                command.Parameters.AddWithValue("@value2", 0);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                dateTable.Rows.Clear();
                adapter.Fill(dateTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                comboBox2.SelectedIndex = comboBox1.SelectedIndex;
                int i = Convert.ToInt32(comboBox2.Text);
                connection = new SqlConnection(connectString);
                string queryInsert = "insert into поставки values (@value1, @value2, @value3)";
                command = new SqlCommand(queryInsert, connection);
                command.Parameters.AddWithValue("@value1", i);
                command.Parameters.AddWithValue("@value2", DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                command.Parameters.AddWithValue("@value3", textBox1.Text);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                dateTable.Rows.Clear();
                adapter.Fill(dateTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                comboBox2.SelectedIndex = comboBox3.SelectedIndex;
                int i = Convert.ToInt32(comboBox2.Text);
                connection = new SqlConnection(connectString);
                string queryInsert = "insert into вывоз values (@value1, @value2, @value3)";
                command = new SqlCommand(queryInsert, connection);
                command.Parameters.AddWithValue("@value1", i);
                command.Parameters.AddWithValue("@value2", DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                command.Parameters.AddWithValue("@value3", textBox3.Text);
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
                dateTable.Rows.Clear();
                adapter.Fill(dateTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void But4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                query = "select * from vw_количество_товара1";
                connection = new SqlConnection(connectString);
                connection.Open();
                command = new SqlCommand(query, connection);
                dateTable = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dateTable);
                dataGrid1.ItemsSource = dateTable.DefaultView;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void But3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                query = "select * from vw_количество_товара";
                connection = new SqlConnection(connectString);
                connection.Open();
                command = new SqlCommand(query, connection);
                dateTable = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dateTable);
                dataGrid1.ItemsSource = dateTable.DefaultView;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void But1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                query = "select * from vw_поставки_месяц";
                connection = new SqlConnection(connectString);
                connection.Open();
                command = new SqlCommand(query, connection);
                dateTable = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dateTable);
                dataGrid1.ItemsSource = dateTable.DefaultView;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void But2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                query = "select * from vw_вывоз_месяц";
                connection = new SqlConnection(connectString);
                connection.Open();
                command = new SqlCommand(query, connection);
                dateTable = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(dateTable);
                dataGrid1.ItemsSource = dateTable.DefaultView;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
