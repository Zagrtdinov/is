﻿using System;
using System.Data.SqlClient;
using System.Windows;

namespace склад
{
    /// <summary>
    /// Логика взаимодействия для регистрация.xaml
    /// </summary>
    public partial class регистрация : Window
    {
        SqlConnection connection;
        SqlCommand command;
        string connectString = "server=localhost; database=учёт_склада; integrated security=true";
        public регистрация()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            Hide();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (text4.Text != "" && text5.Text != "")
                {
                    connection = new SqlConnection(connectString);
                    string queryInsert = "insert into сотрудники values (@value1, @value2, @value3, @value4, @value5)";
                    command = new SqlCommand(queryInsert, connection);
                    command.Parameters.AddWithValue("@value1", text1.Text);
                    command.Parameters.AddWithValue("@value2", text2.Text);
                    command.Parameters.AddWithValue("@value3", text3.Text);
                    command.Parameters.AddWithValue("@value4", text4.Text);
                    command.Parameters.AddWithValue("@value5", text5.Text);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                    MainWindow main = new MainWindow();
                    main.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Заполните поля логина и пароля.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
