﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace склад
{
    /// <summary>
    /// Логика взаимодействия для загрузка.xaml
    /// </summary>
    public partial class загрузка : Window
    {
        System.Windows.Threading.DispatcherTimer timer;
        public загрузка()
        {
            InitializeComponent();
            timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(timerTick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }
        int i = 0;
        private void timerTick(object sender, EventArgs e)
        {
            i++;
            //MessageBox.Show(i.ToString());
            if(i==6)
            {
                timer.Stop();
                MainWindow window = new MainWindow();
                window.Show();
                Hide();
            }
        }
    }
}
