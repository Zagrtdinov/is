﻿using System;
using System.Data.SqlClient;
using System.Windows;

namespace склад
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlConnection connection;
        SqlDataReader reader;
        SqlCommand command;
        string connectString = "server=localhost; database=учёт_склада; integrated security=true";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                connection = new SqlConnection(connectString);
                command = new SqlCommand("select * from сотрудники where логин=@login and пароль=@password", connection);
                connection.Open();
                command.Parameters.AddWithValue("@login", login.Text);
                command.Parameters.AddWithValue("@password", password.Password);
                reader = command.ExecuteReader();
                if (reader.HasRows == true)
                {
                    меню меню = new меню();
                    меню.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Пользователь не найден.", "Ошибка авторизации");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            регистрация регистрация = new регистрация();
            регистрация.Show();
            Hide();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
